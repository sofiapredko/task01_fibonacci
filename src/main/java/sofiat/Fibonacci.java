package sofiat;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * task01_Fibonacci.
 *
 * @author Sofia
 */

public class Fibonacci {

    /**
     * Function to check if number is odd.
     *
     * @param num number
     * @return true if number is odd
     */
    private static boolean isOdd(final int num) {
        return num % 2 == 1;
    }

    /**
     * This function print odd and even numbers in given interval.
     *
     * @param m beginning of the interval
     * @param n end of interval
     */
    public static void printOddandEven(final int m, final int n) {
        System.out.println("Odd numbers: ");
        for (int i = m; i <= n; i++) {
            if (isOdd(i)) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
        System.out.println("Even numbers: ");

        for (int i = n; i >= m; i--) {
            if (!isOdd(i)) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }

    /**
     * Function to calculate sum of odd and even numbers in interval.
     *
     * @param m beginning of the interval
     * @param n end of interval
     */
    public static void sumOddandEven(final int m, final int n) {
        int oddSum = 0;
        int evenSum = 0;

        for (int i = m; i <= n; i++) {
            if (isOdd(i)) {
                oddSum += i;
            }
        }
        System.out.println("Sum of odd numbers: " + oddSum);

        for (int i = n; i >= m; i--) {
            if (!isOdd(i)) {
                evenSum += i;
            }
        }

        System.out.println("Sum of even numbers: " + evenSum);

    }

    /**
     * Function to build n Fibonacci numbers and find
     * the biggest odd and even numbers among them
     * and calculate theis persentage in set.
     *
     * @param size size of Fibonacci number set
     */
    public static void numFib(final int size) {
        int f1 = 0;
        int f2 = 0;
        int a = 0;
        int b = 1;
        double perOdd;
        double perEven;

        int oddCount = 0;
        int evenCount = 0;
        try {
            if (size < 1) {
                System.out.println("Wrong size of set");
                System.exit(0);
            }
            for (int i = 0; i < size; i++) {
                System.out.print(b + " ");
                int next = a + b;

                if (isOdd(b) && b > f1) {
                    f1 = b;
                    oddCount++;
                }

                if (!isOdd(b) && b > f2) {
                    f2 = b;
                    evenCount++;
                }

                a = b;
                b = next;
            }

            System.out.println();
            System.out.println("The biggest odd Fibonacci number F1 = " + f1);
            System.out.println("The biggest even Fibonacci number F2 = " + f2);

            perOdd = oddCount * 100 / (double) (oddCount + evenCount);
            perEven = evenCount * 100 / (double) (oddCount + evenCount);

            System.out.println("The persentage of odd Fibonacci numbers  = " + perOdd + "%");
            System.out.println("The persentage of even Fibonacci numbers  = " + perEven + "%");

        } catch (ArithmeticException e) {
            System.out.println("Wrong");
        }
    }


    public static void main(String[] args) {
        try {
            Scanner scan = new Scanner(System.in);
            System.out.println("Enter the interval: ");

            int n1 = scan.nextInt();
            int n2 = scan.nextInt();

            if (n1 > n2) {
                System.out.println("Incorrect interval");
                System.exit(0);
            }

            printOddandEven(n1, n2);
            sumOddandEven(n1, n2);

            System.out.println("Enter the size of Fibonacci set: ");
            int n = scan.nextInt();
            numFib(n);
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }

    }
}

